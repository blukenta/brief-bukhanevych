from django.db import models

# Create your models here.
class Brief (models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    companyName = models.TextField()
    phone = models.TextField()
    email = models.TextField()
    audience = models.TextField()
    productInfo = models.TextField()
    brandLogo = models.BooleanField()
    brandColor = models.BooleanField()
    brandStyle = models.BooleanField()
    webType = models.TextField()
    daterange = models.TextField()
    budgetSite = models.TextField()
    budgetPromotion = models.TextField()


def AddBrief(name, companyName, phone, email, audience, productInfo, brandLogo, brandColor, brandStyle, webType, daterange, budgetSite, budgetPromotion):
    newBrief = Brief()
    newBrief.name = name
    newBrief.companyName = companyName
    newBrief.phone = phone
    newBrief.email = email
    newBrief.audience = audience
    newBrief.productInfo = productInfo
    if brandLogo == "on":
        newBrief.brandLogo = True
    else:
        newBrief.brandLogo = False

    if brandColor == "on":
        newBrief.brandColor = True
    else:
        newBrief.brandColor = False

    if brandStyle == "on":
        newBrief.brandStyle = True
    else:
        newBrief.brandStyle = False

    newBrief.webType = webType
    newBrief.daterange = daterange
    newBrief.budgetSite = budgetSite
    newBrief.budgetPromotion = budgetPromotion
    newBrief.save()

