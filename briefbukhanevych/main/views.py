from django.shortcuts import render, HttpResponse
from django.views.decorators.http import require_POST
from .models import AddBrief


def index(request):
    return render(request, 'main/index.html')


@require_POST
def addNewBrief(request):
    AddBrief(request.POST["name"], request.POST["companyName"], request.POST["phone"], request.POST["email"],
             request.POST["audience"], request.POST["productInfo"], request.POST["brandLogo"],
             request.POST["brandColor"], request.POST["brandStyle"], request.POST["webType"], request.POST["daterange"],
             request.POST["budgetSite"], request.POST["budgetPromotion"])
    return render(request, 'main/result.html')
